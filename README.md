<h1 align="center">
  <br>
   <a href="https://git.ufz.de/FINAM/finam-examples">
    <img src="Images/Logo.png" alt="Logo" width="90" height="90">
  </a>
  <br>
  FINAM Examples
  <br>
</h1>

`FINAM Examples` repository aims to demonstrate the functionality and applicability of FINAM through several example scripts.
<br>
<br>
<h1>Table of Contents</h1>
  <ol>
    <li>
      <a href="#about-finam">About FINAM</a>
    </li>
    <li>
      <a href="#getting-finam-examples">Getting FINAM Examples</a>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#license">License</a></li>
  </ol>

<br>

## About FINAM
`FINAM` is an open-source component-based model coupling framework written in Python for environmental models.

* Find out more information about FINAM in our [home page](https://finam.pages.ufz.de/).
* How to use FINAM? check our [Guide Book](https://finam.pages.ufz.de/finam-book/introduction.html).
* The latest FINAM Core source can be found [here](https://git.ufz.de/FINAM/finam).
* FINAM documentation can be found [here](https://finam.pages.ufz.de/finam/contents.html).
* Further FINAM ralated projects available in our [GitLab group](https://git.ufz.de/FINAM).

<br>

## Getting FINAM Examples
To be able to run the FINAM Examples on your local machine follow these steps:

1. Install FINAM from the Git repository:
   ```sh
   $ pip install git+https://git.ufz.de/FINAM/finam.git
   ```
2. Install FINAM-netCDF Module from the Git repository:
   ```sh
   $ pip install git+https://git.ufz.de/FINAM/finam-netcdf.git
   ```
   FINAM-netCDF Module reads and write spatial and temporal data from and to [NETCDF](https://www.unidata.ucar.edu/software/netcdf/) files. 
   
   Check out [FINAM-netCDF repository](https://git.ufz.de/FINAM/finam-netcdf).

3. Install finam-plot Module from the Git repository:
   ```sh
   $ pip install git+https://git.ufz.de/FINAM/finam-plot.git
   ```

3. Install finam-graph Module from the Git repository:
   ```sh
   $ pip install git+https://git.ufz.de/FINAM/finam-graph.git
   ```

4. Clone FINAM Examples from the Git repository:
   ```sh
   $ git clone https://git.ufz.de/FINAM/finam-examples.git
   ```
<br>

## Usage

Once FINAM Examples has been cloned on your local machine you can access and run all available examples. You can also view the example files directly from the [Git repository](https://git.ufz.de/FINAM/finam-examples).

Examples available:
* [Hargreaves-Samani](https://git.ufz.de/FINAM/finam-examples/-/tree/main/01_Hargreaves-Samani) --
  Example estimating evapotranspiration based on Hargreaves-Samani equation (1985).
* [Data-Visualization](https://git.ufz.de/FINAM/finam-examples/-/tree/main/Data-Visualization) --
  Download and visualize spatial time series data for monthly precipitation and daily temperature.
* [Reservoir-Cascade](https://git.ufz.de/FINAM/finam-examples/-/tree/main/Reservoir-Cascade) --
  A cascade of reservoirs created by chaining multiple reservoir models.

<br>

## License
FINAM is open source software under the LGPL3 license.
