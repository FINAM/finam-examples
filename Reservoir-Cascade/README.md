# Cascading reservoirs

## What it is

A coupling of two simple reservoir models, chained to a cascade.

Inflow is a sine curve with added noise and peaks.

Reservoirs have a maximum volume, and an outflow that scales linearly with the fill level.
If the maximum volume is exceeded, all reservoirs overflow into their outflow.

```text

Inflow -->
        |~~~~~~~|
        | Res 1 |
        |_______|-->
                  |~~~~~~~~~~~~|
                  |    Res 2   |
                  |____________|-->
```

A live time series plot shows flows and fill levels.

## Usage

Run the example with:

```
python src/main.py
```
Play around with these switches and numbers at the top of the file `src/main.py`:

```python
SHOW_GRAPH = False
USER_CONTROL = False

# Properties of upper reservoir
RES_1_VOLUME = 2.5
RES_1_MIN_FLOW = 0.1
RES_1_MAX_FLOW = 0.75

# Properties of lower reservoir
RES_2_VOLUME = 5.0
RES_2_MIN_FLOW = 0.25
RES_2_MAX_FLOW = 0.75
```
