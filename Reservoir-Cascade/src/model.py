"""Reservoir model"""

import finam as fm


class ReservoirModel(fm.TimeComponent):
    """Reservoir model"""

    def __init__(self, start, step, volume, outflow):
        super().__init__()

        if not volume.check("[length] ** 3"):
            raise ValueError(
                f"Volume must have volume dimensionality. Got {volume.dimensionality}."
            )
        if not outflow[0].check("[length] ** 3 / [time]"):
            raise ValueError(
                f"Volume must have volume flow dimensionality. Got {outflow.dimensionality}."
            )
        if not outflow[1].check("[length] ** 3 / [time]"):
            raise ValueError(
                f"Volume must have volume flow dimensionality. Got {outflow.dimensionality}."
            )

        self.time = start
        self.step = step

        self.max_volume = volume.to("m^3")
        self.normal_outflow = outflow[0].to("m^3/s"), outflow[1].to("m^3/s")
        self.curr_volume = 0.0 * fm.UNITS.Unit("m^3")

    @property
    def next_time(self):
        """Predicted next time step"""
        return self.time + self.step

    def _initialize(self):
        self.inputs.add(name="Inflow", time=self.time, grid=fm.NoGrid(), units="m^3/s")
        self.outputs.add(
            name="Outflow", time=self.time, grid=fm.NoGrid(), units="m^3/s"
        )
        self.outputs.add(name="Volume", time=self.time, grid=fm.NoGrid(), units="m^3")

        self.create_connector(pull_data=["Inflow"])

    def _connect(self, start_time):
        push_data = {}
        if self.connector.all_data_pulled:
            outflow = self._calculate(self.connector.in_data["Inflow"])
            push_data["Outflow"] = outflow
            push_data["Volume"] = self.curr_volume.copy()

        self.try_connect(start_time, push_data=push_data)

    def _validate(self):
        pass

    def _update(self):
        self.time = self.next_time

        inflow = self.inputs["Inflow"].pull_data(self.time)

        outflow = self._calculate(inflow)

        self.outputs["Volume"].push_data(self.curr_volume.copy(), self.time)
        self.outputs["Outflow"].push_data(outflow, self.time)

    def _finalize(self):
        pass

    def _calculate(self, inflow):
        delta = self.step.total_seconds() * fm.UNITS.Unit("s")

        self.curr_volume += inflow.item() * delta

        rel_fill = min(self.curr_volume / self.max_volume, 1.0)
        flow = self.normal_outflow[0] + rel_fill * (
            self.normal_outflow[1] - self.normal_outflow[0]
        )

        total_outflow = flow * delta
        total_outflow = min(self.curr_volume, total_outflow)
        self.curr_volume -= total_outflow

        if self.curr_volume > self.max_volume:
            total_outflow += self.curr_volume - self.max_volume
            self.curr_volume = self.max_volume.copy()

        outflow = total_outflow / delta
        return outflow
