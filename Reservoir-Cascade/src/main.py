"""Cascading reservoirs example"""

import datetime as dt
import math
import random

import finam as fm
import finam_graph as fmg
import finam_plot as fmp
import matplotlib.pyplot as plt
from model import ReservoirModel

SHOW_GRAPH = False
USER_CONTROL = False

# Properties of upper reservoir
RES_1_VOLUME = 2.5
RES_1_MIN_FLOW = 0.1
RES_1_MAX_FLOW = 0.75

# Properties of lower reservoir
RES_2_VOLUME = 5.0
RES_2_MIN_FLOW = 0.25
RES_2_MAX_FLOW = 0.75


def gen_inflow(time, start_time):
    """Noisy source flow generator"""
    delta = (time - start_time).days

    sine = 0.1 + 0.2 * (math.sin(delta * 0.05) + 1)
    noise = random.uniform(0.0, 0.2)
    peak = 1.0 if random.uniform(0.0, 1.0) < 0.1 else 0.0

    return (sine + noise + peak) * fm.UNITS.Unit("m^3/d")


if __name__ == "__main__":
    start = dt.datetime(2000, 1, 1)

    source = fm.modules.CallbackGenerator(
        callbacks={
            "Inflow": (
                lambda t: gen_inflow(t, start),
                fm.Info(time=None, grid=fm.NoGrid(), units="m^3/d"),
            )
        },
        start=start,
        step=dt.timedelta(days=1),
    )
    res_1 = ReservoirModel(
        start=start,
        step=dt.timedelta(days=1),
        volume=RES_1_VOLUME * fm.UNITS.Unit("m^3"),
        outflow=(
            RES_1_MIN_FLOW * fm.UNITS.Unit("m^3/d"),
            RES_1_MAX_FLOW * fm.UNITS.Unit("m^3/d"),
        ),
    )
    res_2 = ReservoirModel(
        start=start,
        step=dt.timedelta(days=1),
        volume=RES_2_VOLUME * fm.UNITS.Unit("m^3"),
        outflow=(
            RES_2_MIN_FLOW * fm.UNITS.Unit("m^3/d"),
            RES_2_MAX_FLOW * fm.UNITS.Unit("m^3/d"),
        ),
    )

    plot = fmp.TimeSeriesPlot(
        {
            "Source": "m3/d",
            "Flow 1": "m3/d",
            "Volume 1": "m3",
            "Flow 2": "m3/d",
            "Volume 2": "m3",
        },
        colors=["black", "orange", "green", "red", "blue"],
        title="Cascading reservoirs",
        pos=(0.0, 0.0),
        size=(1.0, 0.45),
    )

    if USER_CONTROL:
        control = fm.modules.UserControl(start=start)

    composition = fm.Composition(
        [source, res_1, res_2, plot] + ([control] if USER_CONTROL else [])
    )
    composition.initialize()

    _ = source["Inflow"] >> fm.adapters.AvgOverTime(step=0) >> res_1["Inflow"]
    _ = res_1["Outflow"] >> res_2["Inflow"]

    _ = source["Inflow"] >> plot["Source"]
    _ = res_1["Outflow"] >> plot["Flow 1"]
    _ = res_1["Volume"] >> plot["Volume 1"]
    _ = res_2["Outflow"] >> plot["Flow 2"]
    _ = res_2["Volume"] >> plot["Volume 2"]

    if SHOW_GRAPH:
        labels = {
            source: "Inflow",
            res_1: "Reservoir 1",
            res_2: "Reservoir 2",
        }

        fmg.GraphDiagram().draw(
            composition,
            labels=labels,
            block=False,
        )

        fmg.GraphDiagram().draw(
            composition,
            details=0,
            labels=labels,
        )

    composition.run(end_time=dt.datetime(2001, 1, 1))

    plt.ion()
    plt.show(block=True)
