"""
An example that downloads global GPCC monthly precipitation,
and shows it in a dynamic plot.
"""

import datetime as dt
import os
import sys
from urllib import request
from urllib.parse import urlparse

import finam as fm
import finam_netcdf as fm_nc
import finam_plot as fmp


def run():
    """Run the composition"""
    data = parse_args()
    path = download(data["url"], "data")

    reader = fm_nc.NetCdfReader(
        path=path,
        outputs={"data": data["layer"]},
        time_var="time",
    )
    plot = fmp.ImagePlot(
        title=data["title"],
        vmin=data["range"][0],
        vmax=data["range"][1],
        cmap="gist_rainbow",
    )
    control = fm.modules.UserControl(start=data["time"][0], step=data["step"])

    comp = fm.Composition([reader, plot, control])
    comp.initialize()

    _ = reader["data"] >> plot["Grid"]

    comp.connect()
    comp.run(end_time=data["time"][1])


def parse_args():
    """Gets the dataset from the command line args"""
    if len(sys.argv) < 2:
        print(f"Usage:\npython main.py <dataset>\nAvailable datasets: {list(DATA)}")
        sys.exit(1)

    if sys.argv[1] not in DATA:
        print(f"No dataset '{sys.argv[1]}'.\nAvailable datasets: {list(DATA)}")
        sys.exit(1)

    return DATA[sys.argv[1]]


def download(url, folder):
    """Downloads a file is it does not exist"""
    file_base = os.path.basename(urlparse(url).path)
    file = os.path.join(folder, file_base)

    if os.path.isfile(file):
        print("File exists, skipping download.")
        return file

    print("Downloading data.")
    if not os.path.exists(folder):
        os.mkdir(folder)

    def show_progress(block, size, total):
        print(
            f"Downloading... {(block*size)/1000000:.2f}/{total/1000000:.2f} MB",
            end="\r",
        )

    request.urlretrieve(url, file, reporthook=show_progress)

    return file


DATA = {
    "precip": {
        "title": "GPCC Monthly Precipitation [mm/month]",
        "url": (
            "https://opendata.dwd.de/climate_environment/GPCC/full_data_monthly_v2022/10/"
            "full_data_monthly_v2022_2011_2020_10.nc.gz"
        ),
        "layer": fm_nc.Layer(var="precip", xyz=("lon", "lat")),
        "time": (dt.datetime(2011, 1, 1), dt.datetime(2020, 12, 1)),
        "range": (0, 1000),
        "step": dt.timedelta(weeks=4),
    },
    "temp": {
        "title": "Daily mean temperature [°C]",
        "url": (
            "https://opendata.dwd.de/climate_environment/CDC/grids_germany/daily/Project_TRY/"
            "air_temperature_mean/TT_201212_daymean.nc.gz"
        ),
        "layer": fm_nc.Layer(var="temperature", xyz=("x", "y")),
        "time": (dt.datetime(2012, 12, 1), dt.datetime(2012, 12, 31)),
        "range": (-20, 20),
        "step": dt.timedelta(days=1),
    },
}


if __name__ == "__main__":
    run()
