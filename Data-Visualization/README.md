# Data visualization

## What it is

This setup downloads data from [DWD open data](https://opendata.dwd.de/),
and shows it in a dynamic plot.

Available datasets:

* `precip` -- GPCC monthly global precipitation, 10 years
* `temp` -- daily mean temperature for germany, 1 month

## Usage

Run the example with:

```
python src/main.py <dataset>
```

Delete folder `data` in case of errors after a failed download.
